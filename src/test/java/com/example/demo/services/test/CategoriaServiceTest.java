package com.example.demo.services.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.model.Categoria;
import com.example.demo.repository.CategoriaRepository;
import com.example.demo.services.CategoriaService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CategoriaServiceTest {

	@Mock
	private CategoriaRepository categoriaRepository;

	@InjectMocks
	private CategoriaService categoriaService;

	@Test
	public void findAll() {
		List<Categoria> categorias = new ArrayList<Categoria>();
		categorias.add(new Categoria());

		when(categoriaRepository.findAll()).thenReturn(categorias);

		List<Categoria> expected = categoriaService.findAll();

		assertEquals(expected, categorias);
		verify(categoriaRepository).findAll();
	}

	
	  @Test
	    public void findById() {
		  Categoria categoria = new Categoria();
		  categoria.setId(89L);

	        when(categoriaRepository.findById(categoria.getId())).thenReturn(Optional.of(categoria));

	       Optional<Categoria> expected = categoriaService.findById(categoria.getId());

	        assertThat(expected.get()).isSameAs(categoria);
	        
	    }
	  
	@Test
	void save() {
		Categoria categoria = new Categoria();
		categoria.setId(1);
		categoria.setNombre("Tecnologia");
		when(categoriaRepository.save(ArgumentMatchers.any(Categoria.class))).thenReturn(categoria);

		Categoria created = categoriaService.save(categoria);

		assertThat(created.getId()).isSameAs(categoria.getId());
		verify(categoriaRepository).save(categoria);
	}

	@Test
	public void DeleteById() {
		Categoria categoria = new Categoria();
		categoria.setNombre("Test");
		categoria.setId((long)1);

		when(categoriaRepository.findById(categoria.getId())).thenReturn(Optional.of(categoria));

		categoriaService.deleteById(categoria.getId());
	}
	

    @Test
    public void update() {
    	Categoria categoria = new Categoria();
    	categoria.setId(89L);
    	categoria.setNombre("Test");

    	Categoria nuevaCategoria = new Categoria();
    	categoria.setNombre("New Test");

        when(categoriaRepository.findById(categoria.getId())).thenReturn(Optional.of(categoria));
        categoriaService.update(categoria.getId(), nuevaCategoria);

        verify(categoriaRepository).save(nuevaCategoria);
        verify(categoriaRepository).findById(categoria.getId());
    }


    @Test
    public void patch() {
    	Categoria categoria = new Categoria();
    	categoria.setId(89L);
    	categoria.setNombre("Test");

    	Categoria nuevaCategoria = new Categoria();
    	categoria.setNombre("New Test");

        when(categoriaRepository.findById(categoria.getId())).thenReturn(Optional.of(categoria));
        categoriaService.update(categoria.getId(), nuevaCategoria);

        verify(categoriaRepository).save(nuevaCategoria);
        verify(categoriaRepository).findById(categoria.getId());
    }
}
