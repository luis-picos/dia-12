package com.example.demo.services.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.model.Categoria;
import com.example.demo.model.Producto;
import com.example.demo.repository.ProductoRepository;
import com.example.demo.services.ProductoService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ProductoServiceTest {

	@Mock
	private ProductoRepository ProductoRepository;

	@InjectMocks
	private ProductoService ProductoService;

	@Test
	public void findAll() {
		List<Producto> productos = new ArrayList<Producto>();
		productos .add(new Producto());

		when(ProductoRepository.findAll()).thenReturn(productos );

		List<Producto> expected = ProductoService.findAll();

		assertEquals(expected, productos );
		verify(ProductoRepository).findAll();
	}

	@Test
	public void findById() {
		Producto producto = new Producto();
		producto.setId(89L);

		when(ProductoRepository.findById(producto.getId())).thenReturn(Optional.of(producto));

		Optional<Producto> expected = ProductoService.findById(producto.getId());

		assertThat(expected.get()).isSameAs(producto);

	}

	@Test
	void save() {
		Producto producto = new Producto();
		producto.setId(1);
		producto.setNombre("Laptop");
		producto.setCategoria(new Categoria());
		when(ProductoRepository.save(ArgumentMatchers.any(Producto.class))).thenReturn(producto);

		Producto created = ProductoService.save(producto);

		assertThat(created.getId()).isSameAs(producto.getId());
		verify(ProductoRepository).save(producto);
	}

	@Test
	public void DeleteById() {
		Producto producto = new Producto();
		producto.setNombre("Laptop");
		producto.setCategoria(new Categoria());

		when(ProductoRepository.findById(producto.getId())).thenReturn(Optional.of(producto));

		ProductoService.deleteById(producto.getId());
	}

	@Test
	public void update() {
		Producto producto = new Producto();
		producto.setNombre("Laptop");
		producto.setCategoria(new Categoria());

		Producto nuevoProducto = new Producto();
		producto.setNombre("New Test");
		producto.setCategoria(new Categoria());

		when(ProductoRepository.findById(producto.getId())).thenReturn(Optional.of(producto));
		ProductoService.update(producto.getId(), nuevoProducto);

		verify(ProductoRepository).save(nuevoProducto);
		verify(ProductoRepository).findById(producto.getId());
	}

	@Test
	public void patch() {
		Producto producto = new Producto();
		producto.setId(89L);
		producto.setNombre("Test");
		producto.setCategoria(new Categoria());
		
		Producto nuevoProducto = new Producto();
		producto.setNombre("New Test");

		when(ProductoRepository.findById(producto.getId())).thenReturn(Optional.of(producto));
		ProductoService.update(producto.getId(), nuevoProducto);

		verify(ProductoRepository).save(nuevoProducto);
		verify(ProductoRepository).findById(producto.getId());
	}
}
