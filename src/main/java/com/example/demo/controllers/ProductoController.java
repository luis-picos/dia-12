package com.example.demo.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Producto;
import com.example.demo.services.ProductoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/productos")
public class ProductoController {

	@Autowired
	private ProductoService productoService;

	@GetMapping
	@ApiOperation(value ="Retorna la lista de productos",notes="Retorna la lista de todos los productos")
	private ResponseEntity<List<Producto>> getAllProductos() {
		return ResponseEntity.ok(productoService.findAll());
	}

	@GetMapping("/{id}")
	@ApiOperation(value ="Retorna el producto deacuerdo al ID ingresado",notes="Retorna el producto deacuerdo al ID ingresado")
	private ResponseEntity<Optional<Producto>> getProducto(@PathVariable("id") long idProducto) {

		return ResponseEntity.ok(productoService.findById(idProducto));
	}

	@PostMapping
	@ApiOperation(value ="Agregar producto",notes="Agregar nueva producto")
	private ResponseEntity<Producto> saveCategoria(@RequestBody Producto producto) {
		try {
			Producto productoSaved = productoService.save(producto);
			return ResponseEntity.created(new URI("/api/productos" + productoSaved.getId())).body(productoSaved);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PutMapping("/{id}")
	@ApiOperation(value ="Actualizar producto",notes="Actualizar registro completo de producto")
	private ResponseEntity<Producto> update(@RequestBody Producto producto, @PathVariable("id") long idProducto) {
		try {
			Producto response = productoService.update(idProducto, producto);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PatchMapping("/{id}")
	@ApiOperation(value ="actualizar producto parcial",notes="Actualiza registro de producto segun valores enviados")
	private ResponseEntity<Producto> patch(@RequestBody Producto producto,
			@PathVariable("id") long idProducto) {
		try {
			Producto response = productoService.patch(idProducto, producto);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value ="Eliminar producto",notes="Elimina registro de producto deacuerdo a su ID")
	private ResponseEntity<Boolean> delete(@PathVariable("id") long idProducto) {
		productoService.deleteById(idProducto);
		return ResponseEntity.ok((productoService.findById(idProducto) != null));
	}

}
